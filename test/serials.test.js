'use strict';

var t = require('chai').assert;
var serials = require('../lib/serials');

describe('serials', function () {
  it('#detectSync', function () {
    var result = serials.detectSync('tty*');
    t.notOk(result.error);
    t.ok(result.device);
  });
});